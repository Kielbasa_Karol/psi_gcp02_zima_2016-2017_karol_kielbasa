package zagadnienie4;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.neural.data.NeuralData;
import org.encog.neural.data.NeuralDataPair;
import org.encog.neural.data.NeuralDataSet;
import org.encog.neural.data.basic.BasicNeuralDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;

import Rozpoznawanie.Rozpoznawanie;

public class Rozpoznawanie3 {

	static double INPUT[][] = {
			{ 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, // 1
					1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, // 1
					0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, // 0
					1, 0, 1, 1, 1, 1, 0, 1, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, // 0
					1, 1, 1, 1, 0, 1, 0, 0, 0, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, // 0
					1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, // 0
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 }, };

	public static double OUTPUT[][] = { { 1 }, { 1 }, { 1 }, { 1 }, { 1 }, { 1 }, { 1 }, { 1 }, { 0 }, { 1 }, { 0 },
			{ 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 } };

	public static double WALIDACJA[][] = {
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
					1, },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
					1, },
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1,
					1, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, }

	};

	public static double OUTPUT_WALIDACJA[][] = { { 1 }, { 1 }, { 0 }, { 0 }, { 0 }, { 1 }, { 0 }, { 1 }, { 0 }, { 0 },
			{ 1 }, { 1 } };

	static PrintWriter zapis = null;

	/*public static void main(final String args[]) {

		BasicNetwork network = new BasicNetwork();
		network.addLayer(new BasicLayer(null, true, 35));// input + bias
		//network.addLayer(new BasicLayer(new ActivationSigmoid(), true, 15)); // hidden
																				// +
																				// bias
		network.addLayer(new BasicLayer(new ActivationSigmoid(), false, 1)); // output
		network.getStructure().finalizeStructure();
		network.reset();

		try {
			zapis = new PrintWriter("wykres1.txt");
		} catch (FileNotFoundException e) {
			System.out.println("BLAD PRZY TWORZENIU PLIKU");
		}

		NeuralDataSet  trainingSet = new BasicNeuralDataSet (INPUT, OUTPUT);
		NeuralDataSet  trainingSet_oja = new BasicNeuralDataSet  (INPUT, OUTPUT);
		NeuralDataSet  validtationSet = new BasicNeuralDataSet (WALIDACJA, OUTPUT_WALIDACJA);
		
		HebbianTraining1 train = new HebbianTraining1(network, trainingSet, false,1);
		
		long start = 0;
		long stop = 0;

		double executionTime = 0.0;
		int epoch = 1;

		wypiszInput();
		
	
		start = System.currentTimeMillis();

		do {
			train.iteration();
			System.out.println("Epoch " + epoch + " Error:" + train.getError());
			epoch++;
			zapis.println(train.getError());
		} while (train.getError() > 0.1 && epoch < 1000);
		train.finishTraining();
		
		stop = System.currentTimeMillis();
		executionTime = stop - start;
		
		
		int i = 0;
		System.out.println("Czas uczenia: " + executionTime+"ms");
		System.out.println();
		System.out.println("WALIDACJA");
		for (NeuralDataPair  pair : validtationSet) {
			NeuralData  output = network.compute(pair.getInput());
			
			for (int j = 0; j < 35; j++) {
				System.out.print((int)Rozpoznawanie3.WALIDACJA[i][j]);
				if (j % 5 == 4) {
					System.out.println();
				}
			}
			if(i<10)
				i++;
			System.out.println("wynik = " + output.getData(0) + " oczekiwany output = " + pair.getIdeal().getData(0));
		}
		
		Encog.getInstance().shutdown();
		zapis.close();
	}

	private static void wypiszInput() {
		System.out.println("Input ");
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 35; j++) {
				System.out.print((int)INPUT[i][j]);
				if (j % 5 == 4) {
					System.out.println();
				}
			}

			System.out.println();
		}
	}*/
}