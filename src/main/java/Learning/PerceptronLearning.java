package Learning;

import org.encog.engine.network.activation.ActivationBiPolar;
import org.encog.neural.networks.BasicNetwork;

import Sample.Zagadnienie1;

public class PerceptronLearning {

	private BasicNetwork network;
	private ActivationBiPolar funkcjaAktywacji;
	private double alpha;

	public PerceptronLearning(BasicNetwork network, ActivationBiPolar funkcjaAktywacji, double learningRate) {
		this.network = network;
		this.funkcjaAktywacji = funkcjaAktywacji;
		this.alpha = learningRate / 1000;
	}

	public void perceptronLearning() {
		int epoch = 1;
		double result =0;// MSE
		double wage0 = network.getWeight(0, 0, 0);
		double wage1 = network.getWeight(0, 1, 0);

		network.setBiasActivation(1); // ustawiam bias na 1
		int y; // wynik po przejsciu inputu przez funkcje aktywacji
		int maxIterations = 100;

		boolean error = true;
		while (epoch < maxIterations && error == true) {
			error = false;
			System.out.println();
			System.out.println("epoka: " + epoch);
			for (int i = 0; i < 4; i++) {

				double x0 = Zagadnienie1.AND_INPUT[i][0];
				double x1 = Zagadnienie1.AND_INPUT[i][1];

				if (((wage0 * x0) + (wage1 * x1) - network.getLayerBiasActivation(0)) < 0) {
					y = 0;
				} else {
					y = 1;
				}

				if (y != Zagadnienie1.AND_IDEAL[i][0]) {
					error = true;
					wage0 = wage0 + alpha * (Zagadnienie1.AND_IDEAL[i][0] - y) * x0 / 2;
					wage1 = wage1 + alpha * (Zagadnienie1.AND_IDEAL[i][0] - y) * x1 / 2;
					result =+ (Zagadnienie1.AND_IDEAL[i][0] - y)*(Zagadnienie1.AND_IDEAL[i][0] - y);
				}
				
				//System.out.println("waga pierwszego neuronu po zmianie wag: " + wage0);
			//	System.out.println("waga drugiego neuronu po zmianie wag: " + wage1);
			
				System.out.println("MSE:" + (result/epoch));
			}
			
			epoch++;
		}
		sprawdzPoprawnosc(wage0, wage1);
	}

	private void sprawdzPoprawnosc(double wage0, double wage1) {

		for (int i = 0; i < 4; i++) {
			
			double x0 = Zagadnienie1.AND_INPUT[i][0];
			double x1 = Zagadnienie1.AND_INPUT[i][1];
			int y;
			
			if (((wage0 * x0) + (wage1 * x1) - network.getLayerBiasActivation(0)) < 0) {
				y = 0;
			} else {
				y = 1;
			}
			System.out.println();
			System.out.println("x0: " + x0 +" x1: "+x1 + " output: "+ y);
		}
	}
}
