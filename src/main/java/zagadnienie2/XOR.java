package zagadnienie2;
 
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.back.Backpropagation;

public class XOR{

	public static double XOR_INPUT[][] = { { 0.0, 0.0 }, { 1.0, 0.0 },
			{ 0.0, 1.0 }, { 1.0, 1.0 } };

	public static double XOR_IDEAL[][] = { { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
	
	static PrintWriter zapis = null;
	
	public static void main(final String args[]) {
		
		BasicNetwork network = new BasicNetwork();
		network.addLayer(new BasicLayer(null,true,2));// input + bias
		network.addLayer(new BasicLayer(new ActivationSigmoid(),true,3)); // hidden + bias
		network.addLayer(new BasicLayer(new ActivationSigmoid(),false,1)); // output
		network.getStructure().finalizeStructure();
		network.reset();

		

		try {
			zapis = new PrintWriter("wykres1.txt");
		} catch (FileNotFoundException e) {
			System.out.println("BLAD PRZY TWORZENIU PLIKU");
		}
		
		
		MLDataSet trainingSet = new BasicMLDataSet(XOR_INPUT, XOR_IDEAL);
		
		Backpropagation train = new Backpropagation(network, trainingSet,0.3,0);

		long start = 0;
		long stop = 0;

		double executionTime = 0.0;
		int epoch = 1;
		
		start = System.currentTimeMillis();
		
		do {
			train.iteration();
			System.out.println("Epoch " + epoch + " Error:" + train.getError());
			epoch++;
			zapis.println(train.getError());
		} while(train.getError() > 0.1 && epoch < 10000);
		train.finishTraining();
		
		stop = System.currentTimeMillis();
		
		executionTime = stop - start;

		for(MLDataPair pair: trainingSet ) {
			MLData output = network.compute(pair.getInput());
			System.out.println(pair.getInput().getData(0) + "," + pair.getInput().getData(1)
					+ ", actual=" + output.getData(0) + ",ideal=" + pair.getIdeal().getData(0));
		}
		System.out.println("Execution time: "+executionTime);
		Encog.getInstance().shutdown();
		zapis.close();
	}
}