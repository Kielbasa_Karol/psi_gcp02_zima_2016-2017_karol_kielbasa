package Projekt.Main;

import GUI.GUI;
import Projekt.Data.Data;
import Projekt.Zagadnienie1.Zadanie1;
import Projekt.Zagadnienie2.Zadanie2;
import Projekt.Zagadnienie3.Zadanie3;
import Projekt.Zagadnienie4.Zadanie4;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
	
	private Stage window;
	
	public static void main(String[] args) {
		launch(args);
		
	}

	@Override
	public void start(Stage arg0) throws Exception {
		window = arg0;
		Data data = new Data();
		Zadanie1 z1 = new Zadanie1(data);
		Zadanie2 z2 = new Zadanie2(data);
		Zadanie3 z3 = new Zadanie3(data);
		Zadanie4 z4 = new Zadanie4(data);
		GUI gui = new GUI(window,data,z2);
		window.setTitle("ROZPOZNAWANIE LITERY A");
		window.show();
	}

}
