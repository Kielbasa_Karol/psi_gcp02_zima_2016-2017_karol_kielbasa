package Projekt.Data;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;

public class Data {

	private DataSet trainingSet;
	private DataSet validatingSet;
	private DataSet trainingSetForUnsupervised;
	public Data() {
		initTrainingDataSet();
		initValidatingDataSet();
		initTrainingDataSetForUnsupervisedLearning();
	}

	private void initTrainingDataSetForUnsupervisedLearning() {
		trainingSetForUnsupervised = new DataSet(35, 1);
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSetForUnsupervised.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
	}

	private void initValidatingDataSet() {
		validatingSet = new DataSet(35, 1);
		validatingSet.addRow(new DataSetRow(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new double[] { 0 }));
		validatingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1,
				1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		validatingSet.addRow(new DataSetRow(new double[] { 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1,
				1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		validatingSet.addRow(new DataSetRow(new double[] { 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1,
				1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new double[] { 0 }));
		validatingSet.addRow(new DataSetRow(new double[] { 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1,
				1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 0 }));
	}

	private void initTrainingDataSet() {
		trainingSet = new DataSet(35, 1);
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 0 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 0 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1 }, new double[] { 0 }));
		trainingSet.addRow(new DataSetRow(new double[] { 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
		trainingSet.addRow(new DataSetRow(new double[] { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1,
				0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, new double[] { 1 }));
	}
	
	public void printMatrix(double [] output){
		for(int i=0;i<output.length;i++)
		{
			if(i%5 ==4){
				System.out.print((int)output[i]+ " ");
				System.out.println();
			}
			else System.out.print((int)output[i]+ " ");
			
		}
	}

	/*
	 * Getters
	 * 
	 */

	public DataSet getTrainingSet() {
		return trainingSet;
	}

	public DataSet getValidatingSet() {
		return validatingSet;
	}

	public DataSet getTrainingSetForUnsupervised() {
		return trainingSetForUnsupervised;
	}
}
