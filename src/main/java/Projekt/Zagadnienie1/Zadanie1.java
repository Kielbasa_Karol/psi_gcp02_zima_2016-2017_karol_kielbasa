package Projekt.Zagadnienie1;

import java.util.Arrays;

import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.Perceptron;
import org.neuroph.nnet.learning.PerceptronLearning;

import Projekt.Data.Data;

public class Zadanie1 {
	
	private Perceptron perceptron;
	private PerceptronLearning perceptronLearning;
	
	long start = 0;
	long stop = 0;
	double executionTime = 0.0;
	
	public Zadanie1(Data data){
		System.out.println("PERCEPTRON");
		System.out.println();
		initPerceptron(data);
		valitatePerceptron(data);
	}

	private void valitatePerceptron(Data data) {
		System.out.println();
		for (DataSetRow dataRow : data.getValidatingSet().getRows()) {

			perceptron.setInput(dataRow.getInput());
			perceptron.calculate();

			double[] networkOutput = perceptron.getOutput();
			System.out.println("Input: ");
			data.printMatrix(dataRow.getInput());
			System.out.println(" Output: " + Arrays.toString(networkOutput));
		}
		System.out.println("Execution time:" + executionTime+ " ms");
		System.out.println();
	}

	private void initPerceptron(Data data) {
		perceptron = new Perceptron(35,1);
		
		perceptronLearning = new PerceptronLearning();
		perceptronLearning.setLearningRate(0.1);
		
		perceptronLearning.addListener(new LearningEventListener() {
			@Override
			public void handleLearningEvent(LearningEvent event) {
				PerceptronLearning pl = (PerceptronLearning) event.getSource();
				System.out.println("Epoka: " + pl.getCurrentIteration() + " MSE: " + pl.getTotalNetworkError());
			}
		});
		
		perceptron.setLearningRule(perceptronLearning);
		
		start = System.currentTimeMillis();
		perceptron.learn(data.getTrainingSet());
		stop = System.currentTimeMillis();

		executionTime = stop - start;
	}
}
