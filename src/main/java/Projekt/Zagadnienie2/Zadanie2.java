package Projekt.Zagadnienie2;

import java.util.Arrays;

import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.Adaline;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.SigmoidDeltaRule;

import Projekt.Data.Data;

public class Zadanie2 {
	
	private Adaline adaline;
	private SigmoidDeltaRule deltaRule;
	
	private BackPropagation backpropagation;
	private MultiLayerPerceptron MultiLayerNetwork;
	
	long start = 0;
	long stop = 0;
	double executionTime = 0.0;

	public Zadanie2(Data data){
		System.out.println("ADALINE");
		System.out.println();
		
		initAdaline(data);
		valitateAdaline(data);
		
		System.out.println("MLP WITH BACKPROPAGATION");
		System.out.println();
		
		initBackpropagation(data);
		validateBackpropagation(data);
	
	}

	private void validateBackpropagation(Data data) {
		System.out.println();
		for (DataSetRow dataRow : data.getValidatingSet().getRows()) {

			MultiLayerNetwork.setInput(dataRow.getInput());
			MultiLayerNetwork.calculate();

			double[] networkOutput = MultiLayerNetwork.getOutput();
			System.out.println("Input: ");
			data.printMatrix(dataRow.getInput());
			System.out.println(" Output: " + Arrays.toString(networkOutput));
		}
		System.out.println("Execution time: " + executionTime+ " ms");
		System.out.println();
	}

	private void initBackpropagation(Data data) {
		MultiLayerNetwork = new MultiLayerPerceptron(35, 20, 1);
		
		backpropagation = MultiLayerNetwork.getLearningRule();
		backpropagation.addListener(new LearningEventListener() {
			
			@Override
			public void handleLearningEvent(LearningEvent event) {
				 BackPropagation bp = (BackPropagation) event.getSource();
	             System.out.println(" Epoka: " + bp.getCurrentIteration() + " MSE: " + bp.getTotalNetworkError());
			}
		});
		backpropagation.setLearningRate(0.5);
		backpropagation.setMaxIterations(1000);
		
		start = System.currentTimeMillis();
		MultiLayerNetwork.learn(data.getTrainingSet());
		stop = System.currentTimeMillis();

		executionTime = stop - start;
	}

	private void initAdaline(Data data) {
		adaline = new Adaline(35);
		deltaRule = new SigmoidDeltaRule();
		deltaRule.addListener(new LearningEventListener() {
			@Override
			public void handleLearningEvent(LearningEvent event) {
				SigmoidDeltaRule delta = (SigmoidDeltaRule) event.getSource();
				System.out.println("Epoka: " + delta.getCurrentIteration() + " MSE: " + delta.getTotalNetworkError());
			}
		} );
		adaline.setLearningRule(deltaRule);
		
		start = System.currentTimeMillis();
		adaline.learn(data.getTrainingSet());	
		stop = System.currentTimeMillis();

		executionTime = stop - start;
	}
	
	private void valitateAdaline(Data data) {
		System.out.println();
		for (DataSetRow dataRow : data.getValidatingSet().getRows()) {
			adaline.setInput(dataRow.getInput());
			adaline.calculate();

			double[] networkOutput = adaline.getOutput();
			System.out.println("Input: ");
			data.printMatrix(dataRow.getInput());
			System.out.println(" Output: " + Arrays.toString(networkOutput));
		
		}
		System.out.println("Execution time: " + executionTime + " ms");
		System.out.println();
	}
	
	public boolean validateForGUI(double [] input){
		MultiLayerNetwork.setInput(input);
		MultiLayerNetwork.calculate();
		
		double[] networkOutput = MultiLayerNetwork.getOutput();
		if(networkOutput[0] > 0.8)
			return true;
		else
			return false;
	}
}
