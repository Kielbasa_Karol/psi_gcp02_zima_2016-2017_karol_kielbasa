package Rozpoznawanie;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.encog.engine.network.activation.ActivationBiPolar;
import org.encog.neural.networks.BasicNetwork;

public class PerceptronLearning_R {

	private BasicNetwork network;
	private ActivationBiPolar funkcjaAktywacji;
	private double alpha;
	PrintWriter zapis = null;

	public PerceptronLearning_R(BasicNetwork network, ActivationBiPolar funkcjaAktywacji, double learningRate) {
		this.network = network;
		this.funkcjaAktywacji = funkcjaAktywacji;
		this.alpha = learningRate / 1000;
	}

	public void perceptronLearning() {
		int epoch = 1;
		double result = 0;// MSE
		double wages[] = new double[35];
		double x[] = new double[35];

		try {
			zapis = new PrintWriter("wykres.txt");
		} catch (FileNotFoundException e) {
			System.out.println("BLAD PRZY TWORZENIU PLIKU");
		}

		for (int i = 0; i < 35; i++) {
			wages[i] = network.getWeight(0, i, 0);
		}
		network.setBiasActivation(0.25); // ustawiam bias na 1
		int y; // wynik po przejsciu inputu przez funkcje aktywacji
		int maxIterations = 100;
		long start = 0;
		long stop = 0;
		long suma = 0;

		double czasWykonania = 0.0;
		boolean error = true;
		start = System.nanoTime();
		while (epoch < maxIterations && error == true) {
			error = false;
			System.out.println();
			System.out.println("epoka: " + epoch);
			result = 0;
			for (int i = 0; i < 20; i++) {

				suma = 0;
				for (int j = 0; j < 35; j++)
					x[j] = Rozpoznawanie.INPUT[i][j];
				for (int j = 0; j < 35; j++)
					suma += wages[j] * x[j];

				if ((suma - network.getLayerBiasActivation(0)) < 0) {
					y = 0;
				} else {
					y = 1;
				}

				if (y != Rozpoznawanie.OUTPUT[i][0]) {
					error = true;
					for (int k = 0; k < 35; k++) {
						wages[k] = wages[k] + alpha * (Rozpoznawanie.OUTPUT[i][0] - y) * x[k] / 2;
						result += ((Rozpoznawanie.OUTPUT[i][0] - y) * (Rozpoznawanie.OUTPUT[i][0] - y));
					}

				}

			}
			System.out.println("MSE:" + (result / 20));
			double MSE = (result / 20);
			zapis.println(MSE);
			epoch++;
		}

		stop = System.nanoTime();
		czasWykonania = stop - start;
		System.out.println(czasWykonania + " nanosekund");

		sprawdzPoprawnosc(wages, x);
		zapis.close();
	}

	private void sprawdzPoprawnosc(double wages[], double x[]) {
		zapis.println("mse walidacji");
		double result = 0;

		for (int i = 0; i < 12; i++) {
			double suma = 0;
			for (int j = 0; j < 35; j++)
				x[j] = Rozpoznawanie.WALIDACJA[i][j];
			int y;

			for (int j = 0; j < 35; j++)
				suma += wages[j] * x[j];
			if ((suma - network.getLayerBiasActivation(0)) < 0) {
				y = 0;
			} else {
				y = 1;
			}
			if (y != Rozpoznawanie.OUTPUT_WALIDACJA[i][0]) {
				result += ((Rozpoznawanie.OUTPUT_WALIDACJA[i][0] - y) * (Rozpoznawanie.OUTPUT_WALIDACJA[i][0] - y));
			}
			System.out.println("MSE:" + (result / 12));
			double MSE = (result / 12);

			zapis.println(MSE);
			System.out.println();

			System.out.println("WALIDACJA ");
			for (int j = 0; j < 35; j++) {
				System.out.print(Rozpoznawanie.WALIDACJA[i][j]);
				if (j % 5 == 4) {
					System.out.println();
				}
			}

			System.out.println("1-tak 0-nie");
			System.out.println("rozpoznano ?: " + y);
			//zapis.println("Czy rozpoznano litere "+ (i+1) + "? " + y);
			System.out.println();
		}

	}

}
