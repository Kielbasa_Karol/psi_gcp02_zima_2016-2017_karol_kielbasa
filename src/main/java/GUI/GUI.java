package GUI;

import Projekt.Data.Data;
import Projekt.Zagadnienie2.Zadanie2;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class GUI {
	private static Rectangle2D screenResolution = Screen.getPrimary().getVisualBounds();
	
	public static final double WIDTH = screenResolution.getWidth() / 4;
	public static final double HEIGHT = screenResolution.getHeight() / 2;

	private Data data;
	private CheckBox checkboxes[] =  new CheckBox[35];
	private BorderPane borderpaneMain;
	private BorderPane borderpaneTop;
	private BorderPane borderpaneBottom;
	private GridPane gridpane;
	private VBox vbox;
	private Stage window;
	private Scene scene;
	private Label labeTopl;
	private Label labelBottom;
	private Button checkButton;
	
	private double[] output = new double[35];
	
	Zadanie2 z2;
	
	public GUI(Stage window, Data data, Zadanie2 z2) {
		this.window = window;
		this.data = data;
		this.z2 = z2;
		initCheckBoxes();
		initButton();
		initLabel();
		initLayouts();
		initScene();
		
		window.setScene(scene);
	}
	
	private void initButton() {
		checkButton = new Button("SPRAWDZ");
		checkButton.setPrefSize(WIDTH/3, WIDTH/16);
		
		checkButton.setOnAction(e -> handleButtonAction(e));
	}

	private void initLabel() {
		labeTopl = new Label("Zaznacz litere A");
		labeTopl.setFont(new Font("Arial", 40));
		
		labelBottom = new Label("");
		labelBottom.setFont(new Font("Arial", 20));
	}

	private void initScene() {
		 scene = new Scene(borderpaneMain, WIDTH, HEIGHT);
	}

	private void initLayouts() {
		borderpaneMain = new BorderPane();
		borderpaneMain.setPadding(new Insets(WIDTH/32, WIDTH/32, WIDTH/32, WIDTH/32));
		borderpaneMain.setStyle("-fx-background-color: #cee6ff;");
		
		borderpaneTop = new BorderPane();
		borderpaneMain.setTop(borderpaneTop);
		borderpaneTop.setPadding(new Insets(0, 0, 0,WIDTH/12));
		borderpaneTop.setTop(labeTopl);
		borderpaneTop.setStyle("-fx-background-color: #cee6ff;");
		
		borderpaneBottom = new BorderPane();
		borderpaneBottom.setStyle("-fx-background-color: #cee6ff;");
		
		borderpaneMain.setBottom(borderpaneBottom);
		borderpaneBottom.setPadding(new Insets(0, 0, WIDTH/12, WIDTH/12));
		borderpaneBottom.setTop(labelBottom);
		
		vbox = new VBox();
		vbox.setStyle("-fx-background-color: #cee6ff;");
		vbox.setPadding(new Insets(WIDTH/12, 0, 0, WIDTH/3));
		
		
		gridpane = new GridPane();
		gridpane.setPadding(new Insets(WIDTH/32, 0, WIDTH/32, WIDTH/32 - 5));
		borderpaneMain.setCenter(vbox);
		
		addCheckBoxesToGridPane();
		
		vbox.getChildren().addAll(gridpane,checkButton);
	}

	private void addCheckBoxesToGridPane() {
		int j =0;
		int k =0;
		for(int i =0;i<35;i++){
			gridpane.add(checkboxes[i],j,k,1,1);
			
			if(j == 4){
				j=0; 
				k++;
				if(k == 7 ) k=0;
			}
			else j++;
		}
	}

	private void initCheckBoxes() {
		for(int i=0;i<35;i++){
			checkboxes[i]= new CheckBox();
		}
	}

	private void handleButtonAction(ActionEvent e) {
		for(int i=0;i<35;i++){
			if(checkboxes[i].isSelected())
				output[i] = 1;
			else
				output[i] = 0;
		}
		data.printMatrix(output);
		
		System.out.println(z2.validateForGUI(output));
		if(z2.validateForGUI(output))
			labelBottom.setText("ROZPOZNANO LITERE A");
		else
			labelBottom.setText("NIE ROZPOZNANO LITERY A");
	}
}
