
package zagadnienie5;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.som.SOM;
import org.encog.neural.som.training.basic.BasicTrainSOM;
import org.encog.neural.som.training.basic.neighborhood.NeighborhoodSingle;

public class Zagadnienie5 {

	static double INPUT_20[][] = {
			{ 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, // 1
					1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, // 1
					0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, // 1
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, // 0
					1, 0, 1, 1, 1, 1, 0, 1, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, // 1
					1, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
			{ 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, // 0
					1, 1, 1, 1, 0, 1, 0, 0, 0, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, // 0
					1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, // 0
					1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
					1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 }, };

	public static double INPUT_4[][] = { { 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
			0, 0, 0, 1, 1, 0, 0, 0, 1, },

			{ 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
					1, },

			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },

			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0,
					0, }, };

	public static double WALIDACJA_2[][] = { { 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
			1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, },

			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, },

			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0,
					1, }, };

	public static double WALIDACJA_10[][] = {
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
					1, },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
					1, },
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1,
					1, },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0,
					1, },
			{ 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
					0, } };

	static PrintWriter zapis = null;

	public static void main(String args[]) {
		// create the training set
		MLDataSet training = new BasicMLDataSet(INPUT_4, null);

		// Create the neural network.
		SOM network = new SOM(35, 2);
		network.reset();

		try {
			zapis = new PrintWriter("som.txt");
		} catch (FileNotFoundException e) {
			System.out.println("BLAD PRZY TWORZENIU PLIKU");
		}

		long start = 0;
		long stop = 0;
		double executionTime = 0.0;
		BasicTrainSOM train = new BasicTrainSOM(network, 1, training, new NeighborhoodSingle());

		int iteration = 0;

		start = System.currentTimeMillis();
		for (iteration = 0; iteration <= 10; iteration++) {
			train.iteration();
			System.out.println("Iteration: " + iteration + ", Error:" + train.getError());
			zapis.println(train.getError());
		}
		stop = System.currentTimeMillis();
		executionTime = stop - start;

		System.out.println("Czas uczenia: " + executionTime + "ms");
		System.out.println();
		System.out.println("WALIDACJA");

		int counter = 0;
		MLData[] data = new BasicMLData[20];
		for (int i = 0; i < 3; i++)
			data[i] = new BasicMLData(WALIDACJA_2[i]);

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 35; j++) {
				System.out.print((int) WALIDACJA_2[i][j]);
				if (j % 5 == 4) {
					System.out.println();
				}
			}
			System.out.println("Pattern" + (i + 1) + " winner: " + network.classify(data[i]));
			if (network.classify(data[i]) == 1 ) counter++;
		}

		double[][] backpropagation = new double[counter][35];
		double[][] walidacja = { { 1 },{1 }};
		double[][] walidacja1 = { {0}};
	
		for (int k = 0, i =0; k < 3; k++) {
			if (network.classify(data[k]) == 1)
				backpropagation[i++] = data[k].getData();
				
		}
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		for (int a = 0; a < counter; a++) {
			for (int b = 0; b < 35; b++) {
				System.out.print((int) backpropagation[a][b]);
				if (b % 5 == 4) {
					System.out.println();
				}
			}
			System.out.println();
		}
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");

		BasicNetwork network1 = new BasicNetwork();
		network1.addLayer(new BasicLayer(null, true, 35));
		network1.addLayer(new BasicLayer(new ActivationSigmoid(), false, 1));
		network1.getStructure().finalizeStructure();
		network1.reset();

		MLDataSet backproagationSet =null;
		if(counter == 2)
			backproagationSet = new BasicMLDataSet(backpropagation, walidacja);
		else
			backproagationSet =  new BasicMLDataSet(backpropagation, walidacja1);

		Backpropagation train1 = new Backpropagation(network1, backproagationSet, 0.1, 0);
		int epoch = 0;

		do {
			train1.iteration();
			System.out.println("Epoch " + epoch + " Error:" + train1.getError());
			epoch++;
			zapis.println(train1.getError());
		} while (train1.getError() > 0.1 && epoch < 100);
		train1.finishTraining();

		stop = System.currentTimeMillis();

		executionTime = stop - start;

		System.out.println("Czas uczenia: " + executionTime + "ms");
		System.out.println();

		int i = 0;
		for (MLDataPair pair : backproagationSet) {
			MLData output = network1.compute(pair.getInput());

			for (int j = 0; j < 35; j++) {
				System.out.print((int)backpropagation[i][j]);
				if (j % 5 == 4) {
					System.out.println();
				}
			}
			if (i < 3){
				i++;}
			System.out.println("wynik = " + output.getData(0) + " oczekiwany output = " + pair.getIdeal().getData(0));

			zapis.close();
			Encog.getInstance().shutdown();

		}

	}
}