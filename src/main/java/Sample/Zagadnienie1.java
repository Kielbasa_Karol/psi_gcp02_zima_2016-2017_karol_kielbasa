package Sample;

import org.encog.engine.network.activation.ActivationBiPolar;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;

import Learning.PerceptronLearning;
import Rozpoznawanie.PerceptronLearning_R;
 
public class Zagadnienie1 {
 
	public static double AND_INPUT[][] = { { 0.0, 0.0 }, { 1.0, 0.0 },
			{ 0.0, 1.0 }, { 1.0, 1.0 } };
 
	
	public static double AND_IDEAL[][] = { { 0.0 }, { 0.0 }, { 0.0 }, { 1.0 } };
 
	public static void main(final String args[]) {
 
		wypiszInput();
		
		// tworzenie pustej sieci
		BasicNetwork network = new BasicNetwork();
	
		//funkcja bipolarna 
		ActivationBiPolar funkcjaAktywacji = new ActivationBiPolar(); // tworzenie funkcji aktywacji
		
		/*
		 * new BasicLayer(funkcja aktywacji, czy ma biasa, ilosc neuronow w warstwie) 
		 * 
		 * new BasicLayer(ilosc neuronow w warstwie) w tym wypadku funkcja aktywacji to hyperbolic tangent
		 * 
		 */
		
		//tworzenie warstw 
		BasicLayer inputLayer = new BasicLayer(null, true, 2); 
		BasicLayer outputLayer = new BasicLayer(funkcjaAktywacji,false,1);
		
		// dodanie warstw do sieci
		network.addLayer(inputLayer);
		network.addLayer(outputLayer);
		network.getStructure().finalizeStructure();
		network.reset();
		
		System.out.println(network.getWeight(0, 0, 0) + " waga pierwszego neuronu ");
		System.out.println(network.getWeight(0, 1, 0) + " waga drugiego neuronu ");
		
		new PerceptronLearning(network, funkcjaAktywacji, 100).perceptronLearning();
	}

	private static void wypiszInput() {
		System.out.println("Input AND");
		for(int i=0;i<4;i++){
			for(int j=0;j<2;j++)
			{
				System.out.print(AND_INPUT[i][j] + " ");
			}
			System.out.println();
		}
	}
}